/*
 * Requires
 */
var express = require('express');
var bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;
 
var client = new Client();

var logger = require('./logger');
var utility = require('./helpers/utility_helper');
var fileHelper = require('./helpers/file_helper');
var s3Helper = require('./helpers/s3_helper');

var app = express();
app.use(bodyParser.json());

var port = process.env.PORT || 3036;

const properties = JSON.parse(fileHelper.readFileSynch("properties.json"));

var serverType = process.env.server_type;
if(serverType == undefined || serverType == ""){
	serverType = properties.server.default;
}
logger.logInfo("ServerType = " + serverType);
function getServerProperty(propertyName){
	return properties.server[serverType][propertyName];
}
function getServerBucketName(propertyName){
	return properties[propertyName];
}

const assetsBucketName = getServerBucketName('assetsBucketName');
const wildflyEndpoint = getServerProperty('wildflyEndpoint');
const searchActivePromotionsUrl = properties.searchActivePromotionsUrl;

app.get('/health', function(req, res){
	res.end(process.uptime().toString());
})

app.get('/getResources', function(req, res){
	s3Helper.getItem(assetsBucketName, 'resources/ias/resources.json').then((data) => {
		data = JSON.parse(data);
		res.send(data);
	}).catch(function(error){
		logger.logError('ERROR: ' + JSON.stringify(error));
		res.send(utility.createErrorMessage(error));
	});
})

app.get('/getResourceById', function(req, res){
	var id = req.query.id;

	s3Helper.getItem(assetsBucketName, 'resources/ias/' + id + '/resource.json').then((data) => {
		data = JSON.parse(data);
		res.send(data);
	}).catch(function(error){
		logger.logError('ERROR: ' + JSON.stringify(error) );
		res.send(utility.createErrorMessage(error));
	});
})


app.get('/getPromotions', function(req, res){
	var site = req.query.site || 'home_page';
	logger.logInfo('/getPromotions');
	if(site == null || site == undefined){
		logger.logError('Site is undefined!');
		res.send();
	} else {
		var args = {
			data: {
				"startIndex": 0,
				"searchParams": [
					{
						"fieldName": "website",
						"fieldValue": site,
						"comparisonOperation": "="
					},
					{
						"fieldName": "endDate",
						"greaterThanOrEqual": "now/d",
						"comparisonOperation": "range"
					}
				]
			},
			headers: { 
				"Content-Type": "application/json" 
			}
		};
		 
		client.post(wildflyEndpoint + searchActivePromotionsUrl, args, function (data, response) {
			
			res.end(JSON.stringify(data['results']));
		});
	}
})

app.get('/getPromotionsByDivision', function(req, res){
	var division = req.query.division;
	logger.logInfo('/getPromotionsByDivision');

	if(division == null || division == undefined){
		logger.logError('Site Area is undefined!');
		res.send();
	} else {
		var args = {
			data: {
				"startIndex": 0,
				"searchParams": [
					{
						"fieldName": "division",
						"fieldValue": division.toString().toLowerCase(),
						"comparisonOperation": "="
					},
					{
						"fieldName": "endDate",
						"greaterThanOrEqual": "now/d",
						"comparisonOperation": "range"
					}
				]
			},
			headers: { 
				"Content-Type": "application/json" 
			}
		};
		 
		logger.logInfo(wildflyEndpoint + searchActivePromotionsUrl);
		client.post(wildflyEndpoint + searchActivePromotionsUrl, args, function (data, response) {
			logger.logInfo('test');
			res.end(JSON.stringify(data['results']));
		});
	}
})

app.get('/getPromotionById', function(req, res){
	var id = req.query.id;
	logger.logInfo('/getPromotionById');
	if(id == null || id == undefined){
		logger.logError('Promotion ID is undefined');
		res.send();
	} else {
		var args = {
			data: {
				'startIndex': 0,
				'searchParams': [
					{
						'fieldName': 'id',
						'fieldValue': id.toString(),
						'comparisonOperation': '='
					}
				]
			},
			headers: { 
				"Content-Type": "application/json" 
			}
		};
		 
		logger.logInfo(wildflyEndpoint + searchActivePromotionsUrl);
		client.post(wildflyEndpoint + searchActivePromotionsUrl, args, function (data, response) {
			
			res.end(JSON.stringify(data['results'][0]));
		});
	}
})

app.get('/getTestimonials', function(req, res){
	var site = req.query.site || 'ias';
	s3Helper.getItem(assetsBucketName, 'testimonials/' + site.toLowerCase() + '/testimonials.json').then((data) => {
		data = JSON.parse(data);
		res.send(data);
	}).catch(function(error){
		logger.logError('ERROR: ' + JSON.stringify(error));
		res.send(utility.createErrorMessage(error));
	});
})

app.get('/refreshTestimonialCollection', function(req, res){
	var prefix = 'testimonials/';
	var jsonKeys = [];
	var promiseArray = [];

	s3Helper.listObjectsV2(assetsBucketName, prefix).then(function(list){
		for(var i = 0; i < list.length; i++){
			if(list[i]['Key'].indexOf('.json') != -1){
				jsonKeys.push(list[i]['Key']);
			}
		}

		for(var i = 0; i < jsonKeys.length; i++){
			promiseArray.push(s3Helper.getItem(assetsBucketName, jsonKeys[i]));
		}

		Promise.all(promiseArray).then(function(rawRequests){
			var testimonialRequests = [];
			
			for(var i = 0; i < rawRequests.length; i++){
				rawRequests[i] = JSON.parse(rawRequests[i]);
				rawRequests[i]['key'] = jsonKeys[i];
				rawRequests[i]['author'] = rawRequests[i]['firstName'] + " " + rawRequests[i]['lastName'].substring(0, 1);
				rawRequests[i]['quote'] = rawRequests[i]['testimonial'];
				rawRequests[i]['title'] = '';
				
				testimonialRequests.push(rawRequests[i]);
			}

			
			res.end(JSON.stringify(testimonialRequests, null, 2));
		})
	})
})


// Catch 404 and forward to error handler
app.use(function(req, res, next) {
	logger.logError("404 error");
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Error handler, Stacktrace is displayed
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});

app.listen(port);
logger.logDebug("Listening on port: " + port);