var fs = require('fs');

/**
 * @param {string} data
 */
exports.readFile = function(fileName){
    return new Promise(function (resolve, reject){
       fs.readFile(fileName, 'utf8', function (err, data) {
            if (err) {
                reject(err);
            }
            else{
                resolve(data);
            }
        });
    });
}


/**
 * @param {string} data
 */
exports.readFileSynch = function(fileName){
    return fs.readFileSync(fileName, 'utf8');
}


/**
 * @param {string} data
 */
exports.writeFile = function(fileName, data){
    return new Promise(function (resolve, reject){
       fs.writeFile(fileName, data, function (err) {
            if (err) {
                reject(err);
            }
            else{
                resolve(true);
            }
        });
    });
}
    