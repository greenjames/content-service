/**
 *  @param {object} data
 */
exports.removeEmptyFields = function(data){
	for(key in data){
		if(data[key] == ""){
			delete data[key];
		}
	}
	return data;
}

/**
 *  @param {object, string} errorObject
 */
exports.createErrorMessage = function(errorObject){
	var errorData = {
		"success": false,
		"error": errorObject
	};
	return errorData;
}

/**
 *  @param {object} source
 *  @param {object} destination
 *  This will add all values from source into destination
 */
exports.addDetailsFromSource = function(source, destination){
	if(destination == undefined){
		destination = {};
	}
	if(source == undefined){
		return destination;
	}
    var sourceKeys = Object.keys(source);
    for(var i = 0; i < sourceKeys.length; i++){
        var sourceKey = sourceKeys[i];
		this.setFieldIfValid(source, destination, sourceKey);
    }
    return destination;
}

/**
 *  @param {object} source
 *  @param {object} destination
 *  @param {string} fieldName
 *  @param {object} [altFieldName]
 *  This will add a particular value from source into destination, if the field has a value
 */
exports.setFieldIfValid = function(source, destination, fieldName, altFieldName){
	var fieldValue = source[fieldName];
	if (fieldValue != null && fieldValue != undefined) {
		//If field is a number / object, set the field. Else check if field has data (Length > 0)
		if(typeof fieldValue == "number" || typeof fieldValue == "object" || fieldValue.length > 0){
			destination[fieldName] = fieldValue;
			return;
		}
	}
	if(altFieldName != undefined){
		this.setFieldIfValid(source, destination, altFieldName);
	}
}

/**
 *  @param {array} elementList
 *  @param {string} elementName
 *  @param {string} element
 */
exports.findSelectedIndex = function(elementList, elementName, element) {
	for(var i = 0; i < elementList.length; i++) {
		if(elementList[i][elementName] == element) {
			return i;
		}
	}
	return -1;
}

/**
 *  @param {array} elementList
 *  @param {string} elementName
 *  @param {string} element
 */
exports.findSelected = function(elementList, elementName, element) {
	var index = this.findSelectedIndex(elementList, elementName, element);
	if(index == -1){
		return -1;
	}
	return elementList[index];
}